package com.booking.service;

import java.util.List;

import com.booking.models.*;

public class PrintService {
    public static void printMenu(String title, String[] menuArr){
        int num = 1;
        System.out.println(title);
        for (int i = 0; i < menuArr.length; i++) {
            if (i == (menuArr.length - 1)) {   
                num = 0;
            }
            System.out.println(num + ". " + menuArr[i]);   
            num++;
        }
    }

    public static String printServices(List<Service> serviceList){
        String result = "";
        // Bisa disesuaikan kembali
        for (Service service : serviceList) {
            result += service.getServiceName() + ", ";
        }
        return result;
    }

    // Function yang dibuat hanya sebgai contoh bisa disesuaikan kembali
    public static void showRecentReservation(List<Reservation> reservationList){
        int num = 1;
        System.out.println("                                    Recent Reservation                                   ");
        System.out.printf("========================================================================================================%n");
        System.out.printf("| %-4s | %-4s | %-11s | %-20s | %-15s | %-15s | %-10s |%n",
                "No.", "ID", "Nama Customer", "Service", "Biaya Service", "Pegawai", "Workstage");
        System.out.printf("========================================================================================================%n");
        for (Reservation reservation : reservationList) {
            if (reservation.getWorkstage().equalsIgnoreCase("Waiting") || reservation.getWorkstage().equalsIgnoreCase("In process")) {
                System.out.printf("| %-4s | %-4s | %-11s | %-20s | %-15s | %-15s | %-10s |%n",
                num, reservation.getReservationId(), reservation.getCustomer().getName(), printServices(reservation.getServices()), formatCurrency(reservation.getReservationPrice()), reservation.getEmployee().getName(), reservation.getWorkstage());
                num++;
            }
        }
        System.out.printf("========================================================================================================%n");
        System.out.println();
    }

    public static void showAllCustomer(List<Person> personList){
        List<Customer> customerList = personList.stream()
                .filter(person -> person instanceof Customer)
                .map(person -> (Customer)person)
                .toList();

        int num = 1;
        System.out.println("                                     Data Customer                                        ");
        System.out.printf("==================================================================================================%n");
        System.out.printf("| %-4s | %-10s | %-20s | %-15s | %-15s | %-15s |%n",
                "No.", "ID", "Nama Customer", "Alamat", "Membership", "Uang");
        System.out.printf("==================================================================================================%n");
        for (Customer customer : customerList) {
                System.out.printf("| %-4s | %-10s | %-20s | %-15s | %-15s | %-15s |%n",
                        num, customer.getId(), customer.getName(), customer.getAddress(), customer.getMember().getMembershipName(), formatCurrency(customer.getWallet()));
                num++;
        }
        System.out.printf("==================================================================================================%n");
        System.out.println();
    }

    public static void showAllEmployee(List<Person> personList){
        List<Employee> employeeList = personList.stream()
                .filter(person -> person instanceof Employee)
                .map(person -> (Employee) person)
                .toList();
        int num = 1;
        System.out.println("                              Data Employee                             ");
        System.out.printf("=======================================================================%n");
        System.out.printf("| %-4s | %-10s | %-11s | %-15s | %-15s |%n",
                "No.", "ID", "Nama", "Alamat", "Pengalaman");
        System.out.printf("=======================================================================%n");
        for (Employee employee : employeeList) {
            System.out.printf("| %-4s | %-10s | %-11s | %-15s | %-15s |%n",
                    num, employee.getId(), employee.getName(), employee.getAddress(),employee.getExperience());
            num++;
        }
        System.out.printf("=======================================================================%n");
        System.out.println();
    }

    public static void showHistoryReservation(List<Reservation> reservationList){
        int num = 1;
        System.out.println("                                   History Reservation                                   ");
        System.out.printf("================================================================================================================%n");
        System.out.printf("| %-4s | %-15s | %-11s | %-20s | %-15s | %-15s | %-10s |%n",
                "No.", "ID", "Nama Customer", "Service", "Biaya Service", "Pegawai", "Workstage");
        System.out.printf("================================================================================================================%n");
        for (Reservation reservation : reservationList) {
                System.out.printf("| %-4s | %-15s | %-11s | %-20s | %-15s | %-15s | %-10s |%n",
                        num, reservation.getReservationId(), reservation.getCustomer().getName(), printServices(reservation.getServices()), formatCurrency(reservation.getReservationPrice()), reservation.getEmployee().getName(), reservation.getWorkstage());
                num++;
        }
        System.out.printf("================================================================================================================%n");
        System.out.println();

        double keuntungan = reservationList.stream()
                .filter(reservation -> reservation.getWorkstage().equalsIgnoreCase("finish"))
                .reduce(0.0, (subtotal, resevation) -> subtotal + resevation.getReservationPrice(), Double::sum);

        System.out.println("Total keuntungan: " + formatCurrency(keuntungan));
    }
    public static void showAllServices(List<Service> serviceList){
        int num = 1;
        System.out.println("                            Data Service                                ");
        System.out.printf("==========================================================================%n");
        System.out.printf("| %-4s | %-15s | %-20s | %-15s |%n",
                "No.", "Service ID", "Name", "Harga");
        System.out.printf("==========================================================================%n");
        for (Service service : serviceList) {
            System.out.printf("| %-4s | %-15s | %-20s | %-15s |%n",
                    num, service.getServiceId(), service.getServiceName(),formatCurrency(service.getPrice()));
            num++;
        }
        System.out.printf("==========================================================================%n");
        System.out.println();
    }
    public static String formatCurrency (double uang){
        String result = "Rp. " + String.format("%.2f",uang);
        return result;
    }
}
