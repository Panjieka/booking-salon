package com.booking.service;


import com.booking.models.*;

import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

public class ReservationService {
    public static void createReservation(List<Reservation> reservationList, List<Person> personList, List<Service> serviceList, Scanner input){
        Customer customer = null;
        Employee employee = null;
        List<Service> addServices = new ArrayList<>();

        PrintService.showAllCustomer(personList);

        while (true){
            System.out.println("Masukkan Customer Id di bawah ini: ");
            String customerId = input.nextLine();

            try {
                customer = ValidationService.validateCustomerId(customerId, personList);
                break;
            } catch (Exception e){
                System.out.println("Customer Id tidak ditemukan! Pastikan Memasukkan Customer Id dengan sesuai :");
            }
        }

        PrintService.showAllEmployee(personList);

        while (true){
            System.out.println("Masukkan Employee Id di bawah ini: ");
            String EmployeeId = input.nextLine();

            try {
                employee = ValidationService.validateEployeeId(EmployeeId, personList);
                break;
            } catch (Exception e){
                System.out.println("Employee Id tidak ditemukan! Pastikan Memasukkan Customer Id dengan sesuai :");
            }
        }

        PrintService.showAllServices(serviceList);

        while (true){
            System.out.println("Masukkan Service Id di bawah ini : ");
            String serviceId = input.nextLine();

            try {
                Service service = ValidationService.validateServiceId(serviceId, serviceList);
                if (isAdded(addServices, serviceId)){
                    System.out.println("Service Berhasil Ditambah!");
                } else {
                    addServices.add(service);
                    System.out.println("Ingin menambah service lain? (Y/N)");
                    String option = ValidationService.validateStringInput(input,"\\b[YyNn]\\b", "Masukkan Pilihan Y untuk yes atau N untuk No!:");

                    if (option.equalsIgnoreCase("n")){
                        break;
                    }
                }
            } catch (Exception e){
                System.out.println("Service Tidak ada! Masukkan Kembali Service yang Anda Inginkan: ");
            }
        }
        Reservation addReservation = new Reservation(generateReservationId(reservationList), customer, employee, addServices, "in process");
        reservationList.add(addReservation);
        System.out.println("Booking Berhasil!");
        System.out.println("Total biaya Booking: "+ PrintService.formatCurrency(addReservation.getReservationPrice()));
    }




    public static void editReservationWorkstage(List<Reservation> reservationList, Scanner input){
        PrintService.showRecentReservation(reservationList);
        while (true){
            System.out.println("Masukkan Reservation Id: ");
            String reservationId = input.nextLine();

            try {
                Reservation updateStatusReservation = ValidationService.validateReservationId(reservationId, reservationList);
                if (!updateStatusReservation.getWorkstage().equalsIgnoreCase("in process")){
                    System.out.println("Reservasi Tersebut Sudah Tidak Ada");
                } else {
                    System.out.println("Selaikan? atau Cancel?");
                    String newWorkstage = ValidationService.validateStringInput(input, "^(cancel|finish)$","Hanya Bisa Finish atau Cancel!");
                    updateStatusReservation.setWorkstage(newWorkstage);
                    reservationList.stream()
                            .map(reservation -> reservation.getReservationId().equalsIgnoreCase(reservationId)?updateStatusReservation : reservation);
                    System.out.println("Status Reservasi dengan id : "+ reservationId + "Telah " + newWorkstage);
                    break;
                }
            } catch (Exception e){
                System.out.println("Reservasi Id tidak ada! Masukkan ulang Reservasi Id: ");
            }
        }
    }

    private static String generateReservationId(List<Reservation> reservationList){
        String result = "Rsv-0";
        if (reservationList.size()==0){
            result = "Rsv-01";
            return result;
        }

        Reservation lastreservation = reservationList.get(reservationList.size()-1);
        int numberLastReservation = Integer.parseInt(lastreservation.getReservationId().substring(5));
        int newNumber = numberLastReservation +1;

        result+=newNumber;
        return result;
    }

    private static boolean isAdded (List<Service> addServices, String serviceId){
        return addServices.stream()
                .anyMatch(service -> service.getServiceId().equalsIgnoreCase(serviceId));
    }
}
