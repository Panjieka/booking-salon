package com.booking.service;

import com.booking.models.*;

import java.util.List;
import java.util.Scanner;

public class ValidationService {
    public static boolean validateInput(String input, String regex){
        boolean result = input.matches(regex);

        return result;
    }
    public static String validateStringInput(Scanner input, String regex, String errMessage){
        String option;
        while (true){
            String inputString = input.nextLine();
            if (validateInput(inputString, regex)){
                option = inputString;
                return option;
            } else {
                System.out.println(errMessage);
                System.out.println("Silakan Masukkan kembali Pilihan : ");
            }
        }
    }
    public static int validateNumberInput(Scanner input, String regex, String errMessage){
        int choosenMenu;
        while (true){
            String inputMenu = input.nextLine();
            if (validateInput(inputMenu, regex)){
                choosenMenu = Integer.parseInt(inputMenu);
                return choosenMenu;
            } else {
                System.out.println(errMessage);
                System.out.println("Silakan Masukkan kembali Pilihan : ");
            }
        }
    }

    public static Customer validateCustomerId(String idCustomer, List<Person> personList){
        return (Customer) personList.stream()
                .filter(person -> person instanceof Customer)
                .filter(Customer -> Customer.getId().equalsIgnoreCase(idCustomer))
                .findFirst().orElseThrow();
    }
    public static Employee validateEployeeId(String idEmployee, List<Person> personList){
        return (Employee) personList.stream()
                .filter(person -> person instanceof Employee)
                .filter(Employee -> Employee.getId().equalsIgnoreCase(idEmployee))
                .findFirst().orElseThrow();
    }
    public static Service validateServiceId(String idService, List<Service> serviceList){
        return serviceList.stream()
                .filter(service -> service.getServiceId().equalsIgnoreCase(idService))
                .findFirst().orElseThrow();
    }
    public static Reservation validateReservationId(String idReservation, List<Reservation> reservationList){
        return reservationList.stream()
                .filter(reservation -> reservation.getReservationId().equalsIgnoreCase(idReservation))
                .findFirst().orElseThrow();
    }
}
